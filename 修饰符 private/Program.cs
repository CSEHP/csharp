﻿using System;

namespace 修饰符_private
{
    internal class MyClass
    {
        private int a;
        private int b;
        private int c;

        public MyClass(int va, int vb, int vc)
        {
            a = va;
            b = vb;
            c = vc;
        }

        public int getA { get { return a; } }
        public int getB { get { return b; } }
        public int getC { get { return c; } }

    }
    class Program
    {
        static void Main(string[] args)
        {
            MyClass obj = new MyClass(1, 2, 3);
            Console.WriteLine("{0},{1}", obj.getA, obj.getB);

            Console.ReadKey();
        }
    }
}
