﻿using System;

namespace 泛型和异常8
{
    class Program
    {
        static void fun()
        {
            int x = 5, y = 0;
            try
            {
                x = x / y;
            }
            catch (Exception e) 
            {
                Console.WriteLine("fun:{0}", e.Message);
                throw new FormatException(); 
            }
        }
        static void Main(string[] args)
        {
            try { fun(); }
            catch (Exception e) { Console.WriteLine("Main():{0}", e.Message); }
            Console.ReadKey();
        }
    }
}
