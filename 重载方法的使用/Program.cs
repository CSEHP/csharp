﻿using System;

namespace 重载方法的使用
{
    class AddClass //方法名相同 参数 个数 或类型 不同
    {
        public int addValue(int a,int b)
        {
            return a + b;
        }
        public int addValue(int a, int b,int c)
        {
            return a + b+c ;
        }
        public double addValue(double a, double b)
        {
            return a + b;
        }

        public double addValue(double a, double b,double c)
        {
            return a + b+c;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            AddClass s = new AddClass();
            Console.WriteLine(s.addValue(1,3));
            Console.WriteLine(s.addValue(2,5,3));
            Console.WriteLine(s.addValue(2.5,5.2));
            Console.WriteLine(s.addValue(2.6,5.4,5.4));
            Console.ReadKey();
        }
    }
}
