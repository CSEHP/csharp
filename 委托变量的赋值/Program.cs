﻿using System;

namespace 委托变量的赋值
{
    delegate int MyDel(int x, int y); // 声明一个委托类型

    class Program
    {
        static void Main(string[] args)
        {
            MyDel d1 = NamedMethod; //有名字的方法 赋值给委托变量

            //匿名方法 赋值给 委托类
            MyDel d2 = delegate (int x, int y)
            {
                return x + y;
            };
            //lamada 表达式 赋值给委托类
            MyDel d3 = (x, y) => x + y;
            Console.WriteLine(d1(3, 5));
            Console.WriteLine(d2(3, 5));
            Console.WriteLine(d3(3, 5));
            Console.ReadKey();
        }
        private static int NamedMethod(int x, int y) { return x + y; }
    }
}
