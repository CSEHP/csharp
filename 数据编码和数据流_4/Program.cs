﻿using System;
using System.IO;
using System.Text;

namespace 数据编码和数据流_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "201908010000张三";
            Console.WriteLine("写入内存的数据:{0}", str);
            byte[] data = Encoding.Default.GetBytes(str);
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(data, 0, data.Length);
                byte[] bytes = new byte[data.Length];
                ms.Position = 0;
                ms.Read(bytes, 0, bytes.Length);
                string s = Encoding.Default.GetString(bytes);
                Console.WriteLine("从内存读取的数据：" + s);
            }

            Console.ReadKey();
        }
    }
}
