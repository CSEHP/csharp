﻿using System;
using System.Net.Sockets;
using System.Text;

namespace UDPClent_02
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient udpClient = new UdpClient();
            udpClient.Connect("127.0.0.1",10000);
            while (true) {
                System.Console.WriteLine("发送消息");
                string message = Console.ReadLine();
                byte[] data = Encoding.UTF8.GetBytes(message);
                udpClient.Send(data,data.Length);
            }
        }
    }
}
