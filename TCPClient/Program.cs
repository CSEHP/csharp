﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCPClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            for (int i = 0; i < 4; i++)
            {
                TcpClient client = new TcpClient();
                client.Connect(IPAddress.Parse("127.0.0.1"), 2500);
                byte[] buffer = { };

                if (i == 0) buffer = Encoding.Default.GetBytes("202015090046");
                else if (i == 1) buffer = Encoding.Default.GetBytes("万胜杰");
                else if (i == 2) buffer = Encoding.Default.GetBytes(r.Next(100, 200).ToString());
                else if (i == 3) buffer = Encoding.Default.GetBytes(DateTime.Now.ToString());
                NetworkStream ns = client.GetStream();
                ns.Write(buffer, 0, buffer.Length);
                client.Close();
            }
            Console.Read();


        }
    }
}
