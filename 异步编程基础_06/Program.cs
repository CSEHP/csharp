﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace 异步编程基础_06
{
    class Program
    {
        static void Main(string[] args)
        {
            PrepareWorkAsync().Wait();
            Console.WriteLine("起锅烧油，烹制辣子鸡丁... ");
            Console.WriteLine("辣子鸡丁制作完成! ");
            Console.ReadKey();
        }
        private static async Task PrepareWorkAsync()
        {
            Task pepperTask = PepperAsync();
            Task vegetableTask = VegetablesAsync();
            Task chickenTask = ChickenAsync();
            List<Task> tasks = new List<Task> { pepperTask, vegetableTask, chickenTask };
            while (tasks.Count > 0)
            {
                Task completedTask = await Task.WhenAny(tasks);
                if (completedTask == pepperTask) Console.WriteLine("辣椒任务完成");
                else if (completedTask == vegetableTask) Console.WriteLine("蔬菜任务完成");
                else Console.WriteLine("鸡丁任务完成");
                tasks.Remove(completedTask);
            }
        }

        private static async Task ChickenAsync()
        {
            Console.WriteLine("正在腌制鸡丁...");
            await Task.Delay(2000);
        }
        private static async Task VegetablesAsync()
        {
            Console.WriteLine("正在用水泡洗蔬菜...");
            await Task.Delay(2000);
        }
        private static async Task PepperAsync()
        {
            Console.WriteLine("正在用水泡洗辣椒...");
            await Task.Delay(2000);
        }


    }
}
