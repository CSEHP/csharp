﻿using System;

namespace 泛型和异常5
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int x = int.Parse("9999999999");
            }
            catch (ArgumentNullException e) { Console.WriteLine(1); }
            catch (FormatException e) { Console.WriteLine(2); }
            catch (OverflowException e) { Console.WriteLine(3); }
            catch { Console.WriteLine(4); }
            finally { Console.WriteLine("执行filally 语句块！！"); }
            Console.ReadKey();
        }
    }
}
