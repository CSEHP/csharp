﻿using System;
using System.Threading;

namespace 线程_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(Method) { Name = "t1", IsBackground = true };
            Thread t2 = new Thread(Method) { Name = "t2", IsBackground = true };

            t1.Start(); t2.Start(); t2.Join();
            Console.WriteLine("test");
            Console.ReadKey();
        }
        private static void Method()
        {
            for (int i = 0; i < 6; i++)
            {
                //Console.WriteLine("{0}\t{1}", Thread.CurrentThread.Name, i);
                Console.WriteLine($"{Thread.CurrentThread.Name}\ti={i}");
            }
        }

    }
}
