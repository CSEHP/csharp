﻿using System;

namespace 类的构造函数重载形式
{
    class Test1
    {
        public Test1(string str) { Console.WriteLine(str);}
        public Test1() { Console.WriteLine(this); }
        public Test1(int i) { Console.WriteLine(i); }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Test1[] t = new Test1[3];
            t[0] = new Test1();
            t[1] = new Test1("This is String");
            t[2] = new Test1(100);
            Console.ReadKey();
        }
    }
}
