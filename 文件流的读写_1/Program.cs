﻿using System;



using System.IO;
namespace 文件流的读写_1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (FileStream fs = new FileStream("test.txt", FileMode.Create, FileAccess.Write))
            {


                Random r = new Random();
                byte[] data = new byte[200];
                //用随机数填充字节数组data
                r.NextBytes(data);
                fs.Write(data, 0, data.Length);
            }
            using (FileStream fs = new FileStream("test.txt", FileMode.Open, FileAccess.Read))
            {
                //缓存大小是16字节
                byte[] buffer = new byte[16];
                int num = fs.Read(buffer, 0, buffer.Length);
                while (num > 0)
                {
                    Console.Write(BitConverter.ToString(buffer, 0, num));
                    Console.WriteLine();
                    num = fs.Read(buffer, 0, buffer.Length);
                }
            }

            Console.ReadKey();
        }
    }
}
