﻿using System;

namespace 泛型和异常7
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int x = int.Parse(null);
                throw new DivideByZeroException();
            }
            catch (ArgumentNullException e) { Console.WriteLine(e.Message); }
            catch (FormatException e) { Console.WriteLine(e.Message); }
            catch (OverflowException e) { Console.WriteLine(e.Message); }
            catch { Console.WriteLine(1); }
            finally { Console.WriteLine("执行filally 语句块！！"); }
            Console.ReadKey();
        }
    }
}
