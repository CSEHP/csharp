﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TCPServer_04
{
    class Program
    {
        static void Main(string[] args)
        {


            IPAddress ip = IPAddress.Parse("127.0.0.1");
            TcpListener myListener = new TcpListener(ip,12345);
            myListener.Start();
            Console.WriteLine(" server at 127.0.0.1:12345 listening...");
            while (true)
            {
                {
                    Console.Write(" waiting connection... ");
                    TcpClient client = myListener.AcceptTcpClient();
                    Console.WriteLine(" Connection Accepted!");
                    NetworkStream ns = client.GetStream();
                    BinaryWriter bw = new BinaryWriter(ns);
                    try { bw.Write("Hello from Server!"); }

                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadKey();
                        break;
                    }



                }
            }
        }
    }
}