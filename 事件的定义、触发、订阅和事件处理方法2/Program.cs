﻿using System;

namespace 事件的定义_触发_订阅和事件处理方法
{
    public class Teacher
    {
        private string tname;
        public delegate void delegateType();
        public event delegateType ClassEvent;
        public Teacher(string name) { this.tname = name; }
        public void Start()
        {
            Console.WriteLine(tname + "老师宣布开始上课：");
            if (ClassEvent != null) { ClassEvent(); }
        }
    }

    public class Student
    {
        private string sname;
        public Student(string name) { this.sname = name; }
        public void Listener() { Console.WriteLine("学生：" + sname + "正在认真听课"); }
        public void Record() { Console.WriteLine("学生：" + sname + "正在做笔记"); }
        public void Reading() { Console.WriteLine("学生：" + sname + "正在任真看书"); }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Teacher t = new Teacher("万");
            Student s1 = new Student("陈同学");
            Student s2 = new Student("徐同学");
            Student s3 = new Student("张同学");

            t.ClassEvent += new Teacher.delegateType(s1.Listener);
            t.ClassEvent += new Teacher.delegateType(s2.Reading);
            t.ClassEvent += new Teacher.delegateType(s3.Record);

            t.Start();

            Console.ReadKey();
        }
    }
}
