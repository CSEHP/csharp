﻿using System;
using System.Threading;

namespace 线程_1
{
    class Program
    {
        private static int ticket;


        private static void DoWork()
        {
            Console.Clear();
            Console.WriteLine("总票数:{0}张", ticket);
            Thread[] ts = new Thread[1000];
            for (int i = 0; i < 1000; i++)
            {
                ts[i] = new Thread(Shell);
                ts[i].Name = "线程" + i;
            }
            foreach (Thread t in ts) t.Start();
        }


        private static object obj = new object();

        private static void Shell()
        {
            lock (obj)
            {
                if (ticket > 0)
                {
                    Thread.Sleep(2000);
                    ticket--;
                    Console.WriteLine(Thread.CurrentThread.Name + "售票一张，还剩余{0}张票", ticket);
                }
                {

                }
            }
        }

        static void Main(string[] args)
        {
            while (Console.ReadKey().Key == ConsoleKey.Enter)
            {
                ticket = 10;
                DoWork();
                Console.WriteLine("按下Enter键继续，其他键退出！");
            }
        }
    }
}
