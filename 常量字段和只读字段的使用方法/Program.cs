﻿using System;

namespace 常量字段和只读字段的使用方法
{
    class MyClass
    {
        public const int N = 100;
        private const double M = 3.14d;
        public readonly int f1 = 10;
        private readonly double f2;

        public MyClass()//允许构造函数中 给只读变量赋值
        {
            f1 = 20;
            f2 = 30;
        }

        public void fun()//除构造函数外，不允许其他地方更改只读字段的值
        {
            /*
             f1=200;
             f2+=1;
             */
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            MyClass classs1 = new MyClass();
            Console.WriteLine("f1={0}", classs1.f1);
            Console.ReadKey();
        }
    }
}
