﻿using System;
using System.Diagnostics;
using System.IO;

namespace 进程的启动和退出
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = "D:/test.txt";

            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "notepad.exe";
            if (File.Exists(filePath)) File.Delete(filePath);
            File.AppendAllText(filePath, "hello world");
            info.Arguments = "test.txt";
            info.WorkingDirectory = "D:/";

            Process p = Process.Start(info);
            Console.WriteLine("外部程序的开始执行时间：{0}", p.StartTime);
            p.WaitForExit(8000);
            if (p.HasExited == false)
            {
                Console.WriteLine("由主程序强行终止外部程序的运行！");
                p.Kill();
            }
            else
            {
                Console.WriteLine("由主程序正常退出！");
            }

            Console.WriteLine("外部程序的结束运行时间：{0}", p.ExitTime);
            Console.WriteLine("外部程序的结束运行时间的返回值：{0}", p.ExitCode);
            Console.ReadKey();
        }
    }
}
