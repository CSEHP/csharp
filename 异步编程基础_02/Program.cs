﻿using System;

namespace 异步编程基础_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var t1 = new Tuple<int, int, string>(10, 20, "lisi");
            var t2 = new Tuple<string, string>("k001", "网络编程");
            var t3 = Tuple.Create(" hangsan", 20, 100);
            //Console.WriteLine($"{t1}\n{t2}\n{t3}");
            Console.WriteLine(t1 + "--" + t2 + "--" + t3);
            Tuple<string, int, int>[] students =
            {
                    Tuple.Create("aa", 22,80),
                    Tuple.Create(" 123456789",21, 96),
                    Tuple.Create("aaaa", 21,78),
};
            foreach (Tuple<string, int, int> s in students)
            {
                //占位符{0，-10}为左对齐(长度为10)，{0, 10} 为右对齐(长度为10)
                Console.WriteLine("姓名:{0,-10},年龄: {1,-10}, 成绩: {2,-10}", s.Item1, s.Item2, s.Item3);
            }

            Console.ReadKey();

        }
    }
}
