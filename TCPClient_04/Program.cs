﻿using System;
using System.IO;
using System.Net.Sockets;

namespace TCPClient_04
{
    class Program
    {
        static void Main(string[] args)
        {



            try
            {
                Console.WriteLine("Try to connect to 127.0.0.1:12345");
                while (true)
                {
                    TcpClient client = new TcpClient(" 127.0.0.1",12345);
                    NetworkStream ns = client.GetStream();
                    BinaryReader br = new BinaryReader(ns);
                    string message = br.ReadString();
                    Console.WriteLine(message);
                    Console.ReadKey(true);
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                Console.ReadKey();
            }



        }
    }
}
