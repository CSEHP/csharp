﻿using System;

namespace 泛型和异常9
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int x = int.Parse("1230");
            }
            catch (ArgumentNullException e) { Console.WriteLine(e.Message); }
            catch (FormatException e) { Console.WriteLine(e.Message); }
            catch (OverflowException e) { Console.WriteLine(e); }
            finally { Console.WriteLine("执行filally 语句块！！"); }
            Console.ReadKey();
        }
    }
}
