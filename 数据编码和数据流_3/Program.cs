﻿using System;
using System.IO;

namespace 数据编码和数据流_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int len;
            using (FileStream fs = new FileStream("bin.dat", FileMode.Create, FileAccess.ReadWrite))
            {
                BinaryWriter bw = new BinaryWriter(fs);
                //准备不同类型的数据
                int i = 6500001;
                string s = "201915060000李四";
                char[] chars = { 'a', 'b', 'c', '软', '件' };
                len = chars.Length;
                bw.Write(i);
                bw.Write(s);
                bw.Write(chars);
                Console.WriteLine("成功写入各项数据到bin.dat中!");

            }

            using (FileStream fs = new FileStream("bin.dat", FileMode.Open, FileAccess.Read))
            {
                BinaryReader br = new BinaryReader(fs);
                Console.WriteLine("从bin.dat文件中读取各项数据:");
                Console.WriteLine(br.ReadInt32());
                Console.WriteLine(br.ReadString());
                for (int i = 0; i < len; i++)
                {
                    Console.Write("{0}", br.ReadChar());
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
