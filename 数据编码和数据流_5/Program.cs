﻿using System;
using System.IO;

namespace 数据编码和数据流_5
{
    class Program
    {
        static void Main(string[] args)
        {
            using (StreamWriter sw = new StreamWriter("test.txt"))
            {
                DateTime dt1 = new DateTime(2000, 8, 8, 12, 30, 0);
                sw.WriteLine("给定日期：{0}", dt1);
                sw.WriteLine("当前时间：{0}", DateTime.Now);
                sw.WriteLine("201908010000张三");
                sw.WriteLine("201915060000李四");
                Console.WriteLine("写入成功！");
                Console.ReadKey();
            }

            using (StreamReader sr = new StreamReader("test.txt"))
            {
                {
                    Console.WriteLine("读取文件内容：");
                    Console.WriteLine(sr.ReadLine());
                    Console.WriteLine(sr.ReadLine());
                    Console.WriteLine(sr.ReadLine());
                    Console.WriteLine(sr.ReadLine());
                }
            }
        }
    }
}
