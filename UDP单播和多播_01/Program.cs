﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDP单播和多播_01
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint iep = new IPEndPoint(IPAddress.Loopback, 8001);

            UdpClient client = new UdpClient(iep);
            Console.WriteLine("在{0}监听。。。。：", iep);
            IPEndPoint remateEP = null;
            var t1 = Task.Run(() => {

                while (true)
                {
                    var result = client.Receive(ref remateEP);
                    Console.WriteLine("来自{0}：{1}", remateEP, Encoding.Unicode.GetString(result));
                }
            });
            var t2 = Task.Run(async () => {
                while (true)
                {
                    Byte[] bytes = Encoding.Unicode.GetBytes(Console.ReadLine());
                    await client.SendAsync(bytes, bytes.Length, "127.0.0.1", 8002);
                }
            });
            t1.Wait();
            t2.Wait();
        }
    }
}
