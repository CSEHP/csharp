﻿using System;

namespace 泛型和异常6
{
    class Stack<T>//声明栈泛型 
    {
        int maxSize;
            T[]data;
            int top;
            public Stack() // 初始化栈
            {
                maxSize = 10;
                data = new T[maxSize];
                top = -1;
            }
            public bool StackEmpty() { return top == -1; }// 判断栈空的方法
            
            public bool push(T e)  //元素进栈
            {
                if (top == maxSize - 1) return false;
                top++;
                data[top] = e;
                return true;
            }
            public bool Pop(ref T e) // 元素出栈
            {
                if (top == -1) return false;
                e = data[top];
                top--;
                return true;
            }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("出栈次序:");
            int e = 0;
            Stack<int> s1 = new Stack<int>();
            s1.push(1);
            s1.push(2);
            s1.Pop(ref e);
            Console.WriteLine("{0}",e);
            s1.push(3);
            s1.push(4);
            s1.Pop(ref e);
            Console.WriteLine("{0}", e);
            s1.Pop(ref e);
            Console.WriteLine("{0}", e);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
