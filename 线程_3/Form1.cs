﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace 线程_3
{
    public partial class Form1 : Form
    {
        private volatile bool isstop;
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            isstop = false;
            button1.Enabled = false;
            button2.Enabled = true;
            richTextBox1.Text = "";
            Thread t = new Thread(M1);
            t.IsBackground = true;
            t.Start();
        }

        private void M1()
        {
            int i;
            for (i = 0; i < 50 && isstop==false; i++)
            {
                richTextBox1.Invoke(new Action(() => richTextBox1.Text += "a "));
                Thread.Sleep(100);
            }
            if (i == 50) { richTextBox1.Invoke(new Action(() => richTextBox1.Text += "\n写入完成！")); }
            else richTextBox1.Invoke(new Action(() => richTextBox1.Text += "\n写入未完成！"));
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            isstop = true;
            button1.Enabled = true;
            button2.Enabled = false;
        }
    }
}
