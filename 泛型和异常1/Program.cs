﻿using System;

namespace 泛型和异常1
{
    public class GenericList<T> //学生链表泛型类 
    {
        private class Node //结点类
        {
            private Node next;
            private T data;

            public Node(T t)    //构造函数
            {
                next = null;
                data = t;
            }

            public Node Next     // Next 属性
            {
                get { return   next; }
                set { next = value; }
            }

            public T Data      // Data属性
            {
                get { return data; }
                set { data = value; }
            }
        }


        private Node head;
        private Node temp;
        public GenericList()
        {
            head = null;
            temp = null;
        }

        public void AddHead(T t) // 向头部插入一个节点
        {
            Node n = new Node(t);
            n.Next = head;
            head = n;
        }
        public void AddTail(T t) // 向尾部插入一个节点
        {
            Node n = new Node(t);
            if (temp == null) head = n;
            else temp.Next = n;
            temp= n;
        }

        public void Display()// 输出所有节点
        {
            Node current = head;
            while(current !=null)
            {
            Console.WriteLine("{0}",current.Data);
            current = current.Next;
            }
            Console.ReadKey();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            GenericList<int> list = new GenericList<int>();
            list.AddHead(1);
            list.AddHead(2);
            list.AddHead(3);
            list.AddHead(4);
            Console.WriteLine("输出所有节点数据：");
            list.Display();
            
        }
    }
}
