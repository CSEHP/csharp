﻿using System;
using System.IO;
using System.Text;

namespace 数据编码和数据流_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "fs.txt";
            DateTime now = DateTime.Now;
            File.WriteAllText(path, string.Format(" {0:F}\r\n", now), Encoding.Default);
            ReadFromFile(path);
            Console.WriteLine("--------------- ");
            for (int i = 1; i < 5; i++)
            {
                AppendToFile(path, now.AddSeconds(i) + "\r\n");
            }
            ReadFromFile(path);
            Console.ReadKey();
        }

        private static void AppendToFile(string path, string str)
        {
            using (FileStream fs = File.OpenWrite(path))
            {
                byte[] bytes = Encoding.Default.GetBytes(str);
                fs.Position = fs.Length;
                fs.Write(bytes, 0, bytes.Length);
            }


        }

        private static void ReadFromFile(string path)
        {
            StringBuilder sb = new StringBuilder();
            using (FileStream fs = File.OpenRead(path))
            {
                //缓存大小是文件流fs的长度，只需要读取一次就能读完
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                Console.Write(Encoding.Default.GetString(buffer));
            }
        }
    }
}
