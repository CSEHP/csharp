﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace TCPClient_01
{
    class Program
    {
        static async Task Main(string[] args)
        {
            int i = 0;
            await Task.Run(async () =>
            {
                while (true)
                {
                    TcpClient client = new TcpClient();
                    await client.ConnectAsync(IPAddress.Loopback, 2100);
                    Console.WriteLine("第{0}次连接到TCP服务端", ++i);
                    Console.ReadKey(true);
                    client.Close();
                }
            });
            Console.ReadKey();
        }
    }
}
