﻿using System;

namespace 计算GPA
{
    class Program
    {
        class Student               //学生类
        {
            int no;                //学号
            string name;           //姓名
            Course[] course;        //Course类对象数组
            int[] score;           //课程成绩数组
            double gpa1, gpa2;     //常见GPA值和标准GPA值
            public int No         //属性可读可写
            {
                get { return no; }
                set { no = value; }
            }
            public string Name        //属性可读可写
            {
                get { return name; }
                set { name = value; }
            }
            public void SetCourse(Course[] course1)  //设置课程
            {
                course = new Course[course1.Length];
                for (int i = 0; i < course1.Length; i++)
                    course[i] = course1[i];
            }
            public void SetScore(int[] score1)  //设置分数
            {
                score = new int[score1.Length];
                for (int i = 0; i < score1.Length; i++)
                    score[i] = score1[i];
            }
            public void ComputeGpa()    //计算GPA
            {
                int i;
                double s, sumc = 0, sumgpa1 = 0, sumgpa2 = 0;
                for (i = 0; i < score.Length; i++)
                {
                    if (score[i] >= 90) s = 4.0;
                    else if (score[i] >= 80) s = 3.0;
                    else if (score[i] >= 70) s = 2.0;
                    else if (score[i] >= 60) s = 1.0;
                    else s = 0.0;
                    sumgpa1 += course[i].Credits * s;
                    sumgpa2 += course[i].Credits * score[i];
                    sumc += course[i].Credits;
                }
                gpa1 = sumgpa1 / sumc;
                gpa2 = sumgpa2 * 4 / sumc / 100;
            }
            public void DispStud()  //输出学生成绩信息
            {
                Console.WriteLine("学号:{0}\t姓名:{1}", no, name);
                Console.WriteLine("   课程名\t学分\t分数");
                for (int i = 0; i < course.Length; i++)
                    Console.WriteLine("   {0}\t\t{1}\t{2}", course[i].Cname, course[i].Credits, score[i]);
                Console.WriteLine("常见算法GPA={0},标准算法GPA={1}", gpa1, gpa2);
            }
        }
        class Course                          //课程类
        {
            string cname;                    //课程名 
            int credits;                     //课程学分
            public Course(string name, int xf) //课程类的构造函数
            {
                cname = name;
                credits = xf;
            }
            public string Cname             //属性，课程名可读可写
            {
                get { return cname; }
                set { cname = value; }
            }
            public int Credits            //属性，课程学分可读可写
            {
                get { return credits; }
                set { credits = value; }
            }
        }
        static void Main(string[] args)
        {
            Course[] course1 = new Course[] {new Course("英语",4),new Course("数学",3),
                new Course("语文",2),new Course("历史",6),new Course("政治",3)};
            int[] score1 = new int[] { 92, 80, 98, 70, 89 };
            Student s1 = new Student();
            s1.No = 1;
            s1.Name = "王华";
            s1.SetCourse(course1);
            s1.SetScore(score1);
            s1.ComputeGpa();
            s1.DispStud();
            Console.ReadKey();
        }
    }
}
