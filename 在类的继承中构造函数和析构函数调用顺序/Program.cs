﻿using System;

namespace 在类的继承中构造函数和析构函数调用顺序1
{
    class WAN
    {
        private int x;
        public WAN() { Console.WriteLine("调用WAN的构造函数"); }
        public WAN(int x1) { x = x1; Console.WriteLine("调用WAN重载的构造函数"); }
        ~WAN() { Console.WriteLine("WAN:x={0}", x); }
    }

    class SHENG : WAN
    {
        private int y;
        public SHENG() { Console.WriteLine("调用SHENG的构造函数"); }
        public SHENG(int y1, int x1)
            : base(x1)
        {
            y = y1;
            Console.WriteLine("调用SHENG 的重载构造函数");
        }
        ~SHENG() { Console.WriteLine("SHENG:y={0}", y); }
    }

    class JIE : SHENG
    {
        private int z;
        public JIE() { Console.WriteLine("调用JIE的构造函数"); }
        public JIE(int y1, int x1, int z1)
           : base(x1, y1)
        {
            z = z1;
            Console.WriteLine("调用JIE 的重载构造函数");
        }
        ~JIE() { Console.WriteLine("JIE:z={0}", z); }
    }
    class Program
    {
        static void Main(string[] args)
        {
            SHENG S = new JIE();
            Console.ReadKey();

        }
    }
}
