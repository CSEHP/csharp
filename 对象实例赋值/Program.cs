﻿using System;

namespace 对象实例赋值
{
    class MyClass {
        private string str;
        public void setstr(string mystr)
        {
            str = mystr;
         }
        public string getstr() { return str; }

    }
    class Program
    {
        static void Main(string[] args)
        {
            MyClass c1 = new MyClass();
            c1.setstr("marry");
            Console.WriteLine("c1.str={0}",c1.getstr());

            MyClass c2 = new MyClass();
            c2.setstr("weiwei");
            c2 = c1;

            Console.WriteLine("执行c2=c1后");
            Console.WriteLine("c2.str={0}",c2.getstr());
            Console.ReadKey();
           
        }
    }
}
