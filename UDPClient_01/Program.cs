﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient_01
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            Task.Run(() =>
            {
                while (true)
                {
                    System.Net.Sockets.UdpClient client = new System.Net.Sockets.UdpClient();
                    client.Send(new byte[] { }, 0, new IPEndPoint(IPAddress.Loopback, 1800));
                    Console.WriteLine("成功连接{0}次到UDP服务端", ++i);
                    Console.ReadKey(true);
                    Console.Clear();
                }
            }).Wait();
            Console.ReadKey();
        }
    }
}
