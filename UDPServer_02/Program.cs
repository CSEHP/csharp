﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UDPServer_02
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint iep = new IPEndPoint(IPAddress.Parse("127.0.0.1"),10000);
            UdpClient udpClient = new UdpClient(iep);
            while (true) {
                IPEndPoint remoteEp = null;
                byte[] data = udpClient.Receive(ref remoteEp);
                string message = Encoding.UTF8.GetString(data);
                System.Console.WriteLine("收到消息："+message);
            }
        }
    }
}
