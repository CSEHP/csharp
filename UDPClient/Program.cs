﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            //for循环控制发送多少次信息到UDP服务端
            for (int i = 0; i < 4; i++)
            {
                //新建UdeCilent类的实例对象，其IP地址和端口号有系统自动分配
                UdpClient client = new UdpClient();
                byte[] data = { };
                //实用if选择语句控制每次发送内容
                if (i == 0) data = Encoding.Default.GetBytes("201908010000");
                else if (i == 1) data = Encoding.Default.GetBytes("张三");
                else if (i == 2) data = Encoding.Default.GetBytes(r.Next(100, 200).ToString());
                else if (i == 3) data = Encoding.Default.GetBytes(DateTime.Now.ToString());
                //Send同步方法的第3个参数一定是UDP服务端的ip终结点，否则服务器收不到信息
                client.Send(data, data.Length, new IPEndPoint(IPAddress.Loopback, 2100));
                client.Close();


            }

            Console.ReadKey();
        }
    }
}