﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace 线程_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(Method));
            t.IsBackground = true;
            t.Start();
        }

        private void Method()
        {
            AddMessage("abc");
        }
        private void Method(object obj)
        {
            string s = obj as string;
            AddMessage(s);
        }

        delegate void MyDel();
        private void AddMessage(string v)
        {
            this.textBox1.Invoke(new MyDel(()=> { textBox1.Text = v; }));
        }
    }
}
