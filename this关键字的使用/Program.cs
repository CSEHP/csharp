﻿using System;

namespace this关键字的使用
{
    class Student
    {
        private string name;
        private float score;
        public Student(string name, float score) {
            this.name = name;
            this.score = score;
        }

        public void display() {
            Console.WriteLine("name:{0}",name);
            Console.WriteLine("等级={0}",Degree.getDegree(this));
        }
        public float psocore {
            get { return score; }
        }

        class Degree {
            public static string getDegree(Student s) {
                if (s.psocore >= 90)
                    return "优秀";
                else if (s.psocore >= 80) return "良好";
                else if (s.psocore >= 70) return "中等";
                else if (s.psocore >= 60) return "及格";
                else return "不及格";
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student("weiwei",120);
            s.display();
            Console.ReadKey();
        }
    }
}
