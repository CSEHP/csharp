﻿using System;

namespace 委托的使用
{
    delegate double mydelegate(double x, double y); // 声明委托类

    class MyDeClass
    {
        public double add(double x, double y) { return x + y; }
        public double sub(double x, double y) { return x - y; }
        public double mul(double x, double y) { return x * y; }
        public double div(double x, double y) { return x / y; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyDeClass obj = new MyDeClass();
            mydelegate p = new mydelegate(obj.add);// 委托类

            Console.WriteLine("5+8={0}", p(5, 8));
            p = new mydelegate(obj.sub);
            Console.WriteLine("5-8={0}", p(5, 8));
            p = new mydelegate(obj.mul);
            Console.WriteLine("5*8={0}", p(5, 8));
            p = new mydelegate(obj.div);
            Console.WriteLine("5/8={0}", p(5, 8));
            Console.ReadKey();
        }
    }
}
