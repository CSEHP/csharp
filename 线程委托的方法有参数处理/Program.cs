﻿using System;
using System.Threading;

namespace 线程委托的方法有参数处理
{
    class MyClass
    {
        public int sno { get; set; }
        public string sname { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(M1));
            Thread t2 = new Thread(new ParameterizedThreadStart(M2));
            t1.IsBackground = true;
            t1.Start();
            MyClass s1 = new MyClass();
            s1.sname = "张三";
            s1.sno = 1000;
            t2.IsBackground = true;
            t2.Start(s1);

            Console.ReadKey();
        }
        private static void M1()
        {
            Console.WriteLine("test");
        }
        private static void M2(object obj)
        {
            MyClass s = obj as MyClass;
            Console.WriteLine("学号:{0},姓名：{1}", s.sno, s.sname);
        }

    }
}
