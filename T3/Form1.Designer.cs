﻿namespace T3
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbx1 = new System.Windows.Forms.TextBox();
            this.btn_display = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtbx1
            // 
            this.txtbx1.Location = new System.Drawing.Point(48, 78);
            this.txtbx1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtbx1.Name = "txtbx1";
            this.txtbx1.Size = new System.Drawing.Size(289, 25);
            this.txtbx1.TabIndex = 0;
            // 
            // btn_display
            // 
            this.btn_display.Location = new System.Drawing.Point(140, 154);
            this.btn_display.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_display.Name = "btn_display";
            this.btn_display.Size = new System.Drawing.Size(100, 29);
            this.btn_display.TabIndex = 1;
            this.btn_display.Text = "显示时间";
            this.btn_display.UseVisualStyleBackColor = true;
            this.btn_display.Click += new System.EventHandler(this.btn_display_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 245);
            this.Controls.Add(this.btn_display);
            this.Controls.Add(this.txtbx1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbx1;
        private System.Windows.Forms.Button btn_display;
    }
}

