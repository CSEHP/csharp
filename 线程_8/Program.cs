﻿using System;
using System.Diagnostics;
using System.Threading;

namespace 线程_8
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int a = Console.Read();
                char input = (char)a;
                if (input == 'p' || input == 'P') { CreateProc(); CreateProc(); CreateProc(); DispProc(); }
                if (input == 'd' || input == 'D') DispProc();
                if (input == 'k' || input == 'K') KillProc();
                if (input == 'x' || input == 'X') return;
            }
        }
        public static void KillProc()
        {
            Process[] ps = Process.GetProcessesByName("notepad");
            foreach (Process p in ps)
            {
                p.Kill();
                Console.WriteLine("结束进程pid:{0}", p.Id);
                Thread.Sleep(100);
                p.WaitForExit();
                p.Close();
            }
            Console.WriteLine("所有notepad进程已退出");
        }
        public static void CreateProc()
        {
            Process p = new Process();
            p.StartInfo.FileName = "notepad";
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.Start();
        }
        public static void DispProc()
        {
            Console.Clear();
            Process[] ps = Process.GetProcessesByName("notepad");
            foreach (Process p in ps)
            {
                Console.Write("进程ID：" + p.Id + "\t" + "进程启动时间：" + p.StartTime + "\n");
            }
        }
    }
}