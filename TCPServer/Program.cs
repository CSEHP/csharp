﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCPServer
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint iep = new IPEndPoint(IPAddress.Any, 2500);
            TcpListener listener = new TcpListener(iep);
            listener.Start();
            Console.WriteLine("TCP服务器在{0}监听....", iep);

            while (true)
            {
                StringBuilder sb = new StringBuilder();
                TcpClient client = listener.AcceptTcpClient();
                NetworkStream ns = client.GetStream();

                byte[] buffer = new byte[1024];
                int num = ns.Read(buffer, 0, buffer.Length);

                while (num > 0)
                {
                    sb.Append(Encoding.Default.GetString(buffer, 0, num));
                    num = ns.Read(buffer, 0, buffer.Length);
                }

                Console.WriteLine("从{0}发来信息{1}", client.Client.RemoteEndPoint, sb);

            }
            Console.ReadKey();
            listener.Stop();


        }
    }
}
