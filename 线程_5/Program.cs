﻿using System;

namespace 线程_5
{
    class Student
    {
        public string no { get; set; }
        private string name { get; set; }
        private int score { get; set; }

        public Student(string no, string name, int score)
        {
            this.no = no;
            this.name = name;
            this.score = score;
        }
        public override string ToString()
        {
            return $"学号：{no},姓名：{name},成绩：{score}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student("1000", "张三", 100);
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
