﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using Microsoft.Win32;

namespace 网卡信息_和网络流量信息
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0;
            StringBuilder sb = new StringBuilder();
            string hostname = Dns.GetHostName();
            sb.AppendLine("本地计算机名:" + hostname);
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)
            {
                string fCardType = "未知网卡";
                string fRegistryKey = "SYSTEM\\CurrentControlSet\\Control\\Network\\" +
                                      "{4D36E972-E325-11CE-BFC1-08002BE10318}\\" + adapter.Id + "\\Connection";
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(fRegistryKey, false);
                if (rk != null)
                {
                    string fPnpInstanceID = rk.GetValue("PnpInstanceID", "").ToString();
                    int fMediaSubType = Convert.ToInt32(rk.GetValue("MediaSubType", 0));
                    if (fPnpInstanceID.Length > 3 &&
                        fPnpInstanceID.Substring(0, 3) == "PCI" && fMediaSubType != 2)
                        fCardType = "物理网卡";
                    else if (fMediaSubType == 1)
                        fCardType = "虚拟网卡";
                    else if (fMediaSubType == 2)
                        fCardType = "无线网卡";
                }

                if (fCardType == "物理网卡" || fCardType == "无线网卡")
                {
                    count++;
                    sb.AppendLine("-----------------网卡适配器信息-----------------");
                    sb.AppendLine("描述信息:" + adapter.Description);
                    sb.AppendLine("名称:" + adapter.Name);
                    sb.AppendLine("类型:" + adapter.NetworkInterfaceType);
                    sb.AppendLine("网卡速度:" + adapter.Speed / 1000 / 1000 + "Mb/s");
                    sb.AppendLine("MAC地址:" + BitConverter.ToString(adapter.GetPhysicalAddress().GetAddressBytes()));
                    IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
                    GatewayIPAddressInformationCollection gatewayAddresses = adapterProperties.GatewayAddresses;
                    IPAddressCollection dhcpServers = adapterProperties.DhcpServerAddresses;
                    IPAddressCollection dnsServers = adapterProperties.DnsAddresses;
                    UnicastIPAddressInformationCollection unicastIps = adapterProperties.UnicastAddresses;
                    if (unicastIps.Count > 0)
                        foreach (UnicastIPAddressInformation ip in unicastIps)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                sb.Append("IP地址:" + ip.Address + "\n");
                                sb.Append("子网掩码:" + ip.IPv4Mask + "\n");
                            }
                        }

                    if (dhcpServers.Count > 0)
                        foreach (IPAddress ip in dhcpServers)
                        {
                            if (ip.AddressFamily == AddressFamily.InterNetwork)
                                sb.Append("DHCP服务器IP地址:" + ip + "\n");
                        }

                    if (gatewayAddresses.Count > 0)
                        foreach (GatewayIPAddressInformation gateway in gatewayAddresses)
                        {
                            sb.Append("网关:" + gateway.Address + "\n");

                        }

                    if (dnsServers.Count > 0)
                        foreach (IPAddress ip in dnsServers)
                        {
                            sb.Append("DNS服务器IP地址:" + ip + "\n");
                        }
                }
            }

            if (count == 0) return;
            sb.AppendLine("-----------------网络流量信息-----------------");
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPGlobalStatistics ipstat = properties.GetIPv4GlobalStatistics();
            sb.AppendLine("本机所在域......:" + properties.DomainName);
            sb.AppendLine("接收数据包数....:" + ipstat.ReceivedPackets);
            sb.AppendLine("转发数据包数....:" + ipstat.ReceivedPacketsForwarded);
            sb.AppendLine("传送数据包数....:" + ipstat.ReceivedPacketsDelivered);
            sb.AppendLine("丢弃数据包数....：" + ipstat.ReceivedPacketsDiscarded);
            TcpStatistics connections = properties.GetTcpIPv4Statistics();
            sb.AppendFormat("当前tcp连接数:{0}\n", connections.CurrentConnections);
            sb.AppendFormat("本机最大tcp连接数：{0}", connections.MaximumConnections);
            Console.WriteLine(sb.ToString());
            Console.ReadKey();
        }
    }
}