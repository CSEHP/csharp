﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCPClent_02
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient();
            client.Connect(new IPEndPoint(IPAddress.Loopback, 2300));
            NetworkStream ns = client.GetStream();
            BinaryWriter bw = new BinaryWriter(ns);
            BinaryReader br = new BinaryReader(ns);
            Console.WriteLine("连接到服务端的客户端终结点信息: {0}", client.Client.LocalEndPoint);
            string str;

            var t1 = Task.Run(() => {
                while (true)
                {
                    try
                    {
                        str = Console.ReadLine();
                        Console.WriteLine("客户端收到的消息：" + str);
                        bw.Write(str);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadKey();
                        break;
                    }
                }
            });

            var t2 = Task.Run(() => {
                while (true)
                {
                    try
                    {
                        Console.WriteLine("客户端收到的消息：" + br.ReadString());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.ReadKey();
                        break;
                    }
                }
            });

            t1.Wait();
            t2.Wait();
        }
    }
}
