﻿using System;

namespace 泛型和异常2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int x = int.Parse("a134");
            }
            catch (ArgumentNullException e) { Console.WriteLine(e.Message); }
            catch (FormatException e) { Console.WriteLine(e.Message); }
            catch (OverflowException e) { Console.WriteLine(e.Message); }
            finally { Console.WriteLine("执行filally 语句块！！"); }
            Console.ReadKey();
        }
    }
}
