﻿using System;

namespace 事件的定义_触发_订阅和事件处理方法
{
    public delegate void MyDelegate(int c, int n);// 声明 一个事件委托类型

    public class Shape
    {
        protected int color;
        protected int size;

        //定义两个事件
        public event MyDelegate ColorChange;
        public event MyDelegate GetSize;

        public int pcolor
        {
            set
            {
                int ocolor = color; // 保存原来的颜色
                color = value;
                ColorChange(ocolor, color); //设置pcolor 值时 触发事件ColorChange
            }
        }

        public int psize
        {
            get
            {
                GetSize(size, 10); // 获取psize 时 触发事件 GetSize
                return size;
            }
        }

        //构造函数
        public Shape() { color = 0; size = 10; }
        //重载构造函数
        public Shape(int c, int s) { color = c; size = s; }


    }
    class Program
    {
        static void CCHandler1(int c, int n)
        {
            Console.WriteLine("颜色从{0}改变为{1}", c, n);
        }

        static void CCHandler2(int s, int n)
        {
            if (s == n)
            {
                Console.WriteLine("size 大小没变");
            }
            else
            {
                Console.WriteLine(" size 大小已经改变");
            }
        }

        static void Main(string[] args)
        {
            Shape obj = new Shape();

            // 订阅事件
            obj.ColorChange += new MyDelegate(CCHandler1);
            obj.GetSize += new MyDelegate(CCHandler2);

            Console.WriteLine("obj 对象的操作");
            // 触发两个事件
            obj.pcolor = 3;
            Console.WriteLine("size 大小为:{0}", obj.psize);
            Console.WriteLine("================================");
            Shape obj1 = new Shape(5, 20);
            obj1.ColorChange += new MyDelegate(CCHandler1);
            obj1.GetSize += new MyDelegate(CCHandler2);
            Console.WriteLine("obj1 对象操作");
            obj1.pcolor = 3;
            Console.WriteLine("size 大小为:{0}", obj1.psize);

            Console.ReadKey();
        }
    }
}
