﻿using System;

namespace 泛型和异常3
{
    public class GenericList<T> //学生链表泛型类 
    {
        private class Node //结点类
        {
            private Node next;
            private T data;

            public Node(T t)    //构造函数
            {
                next = null;
                data = t;
            }

            public Node Next     // Next 属性
            {
                get { return next; }
                set { next = value; }
            }

            public T Data      // Data属性
            {
                get { return data; }
                set { data = value; }
            }
        }


        private Node head;
        private Node temp;
        public GenericList()
        {
            head = null;
            temp = null;
        }

        public void AddHead(T t) // 向头部插入一个节点
        {
            Node n = new Node(t);
            n.Next = head;
            head = n;
        }
        public void AddTail(T t) // 向尾部插入一个节点
        {
            Node n = new Node(t);
            if (temp == null) head = n;
            else temp.Next = n;
            temp = n;
        }

        public void Display()// 输出所有节点
        {
            Node current = head;
            while (current != null)
            {
                Console.WriteLine("{0}", current.Data);
                current = current.Next;
            }
            Console.ReadKey();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            GenericList<double> list = new GenericList<double>();
            list.AddTail(1.2);
            list.AddTail(2.5);
            list.AddTail(3.4);
            list.AddTail(4.8);
            Console.WriteLine("输出所有节点数据：");
            list.Display();

        }
    }
}
