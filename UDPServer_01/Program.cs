﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPServer_01

{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint iep = new IPEndPoint(IPAddress.Loopback, 1800);
            UdpClient sever = new UdpClient(iep);
            Console.WriteLine("UDP服务端在{0}等待接收信息...", iep);
            Task.Run(() =>
            {
                while (true)
                {
                    IPEndPoint remoteEp = null;
                    sever.Receive(ref remoteEp);
                    Console.WriteLine("UDP客户终端节点信息：{0}", remoteEp);
                }
            });
            Console.ReadKey();
        }
    }
}
