﻿using System;

namespace 事件源类和订阅者类_事件的定义_触发_订阅和事件处理方法
{
    public delegate void EndExamType(DateTime endTime, Student stu);
    public delegate void StartExamType(DateTime startTime);

    public class Student
    {
        private string name;
        public event EndExamType EndExam;
        public Student(string name)
        {
            this.name = name;
        }
        public string pname
        {
            get { return name; }
        }

        public void Testing(DateTime beginTime) { Console.WriteLine("学生{0}在{1}时开始答题...", name, beginTime); }

        public void HandIn() { EndExam(DateTime.Now, this); }
    }
    class Teacher
    {
        public event StartExamType StartExam;
        public void NotifyBeginExam()
        { Console.WriteLine("万老师宣布开始考试"); StartExam(DateTime.Now); }

        public void Accept(DateTime acceptTime, Student stud) { Console.WriteLine("学生" + stud.pname + "完成考试 ，老师收卷"); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Teacher t = new Teacher();
            Student[] s = new Student[5];
            s[0] = new Student("张军");
            s[1] = new Student("陈华");
            s[2] = new Student("王丽");
            s[3] = new Student("张卫");
            s[4] = new Student("刘畅");

            foreach (Student stu in s)
            {
                t.StartExam += new StartExamType(stu.Testing);
                stu.EndExam += new EndExamType(t.Accept);
            }
            t.NotifyBeginExam();
            Console.WriteLine("经过一段时间...");
            s[1].HandIn();
            Console.WriteLine("经过一段时间...");
            s[2].HandIn();
            Console.WriteLine("经过一段时间...");
            s[4].HandIn();
            Console.WriteLine("经过一段时间...");
            s[0].HandIn();
            Console.WriteLine("经过一段时间...");
            s[3].HandIn();
            Console.ReadKey();
        }
    }
}
