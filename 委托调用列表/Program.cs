﻿using System;

namespace 委托调用列表
{
    delegate void mydelegate(double x, double y);

    class MyDeClass
    {
        public void add(double x, double y) { Console.WriteLine("{0}+{1}={2}", x, y, x + y); }
        public void sub(double x, double y) { Console.WriteLine("{0}-{1}={2}", x, y, x - y); }
        public void mul(double x, double y) { Console.WriteLine("{0}*{1}={2}", x, y, x * y); }
        public void div(double x, double y) { Console.WriteLine("{0}/{1}={2}", x, y, x / y); }

    }
    class Program
    {
        static void Main(string[] args)
        {
            MyDeClass obj = new MyDeClass();
            mydelegate p, a;

            a = obj.add;
            p = a;

            a = obj.sub;
            p += a;

            a = obj.mul;
            p += a;

            a = obj.div;
            p += a;

            p(5, 8);

            Console.ReadKey();
        }
    }
}
