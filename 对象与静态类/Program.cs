﻿using System;

namespace 静态字段
{
    class MyClass
    {
        int f1;
        private static int f2;
        public MyClass() { }
        public MyClass(int a, int b)
        {
            f1 = a;
            f2 = b;
        }
        public void display()
        {
            Console.WriteLine("f1:{0},f2:{1}",f1,f2);
            
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            MyClass c1 = new MyClass(2, 5);
            MyClass c2 = new MyClass(3, 6);
            MyClass c3 = new MyClass(4, 7);
            MyClass c4 = new MyClass(5, 6);
            MyClass c5 = new MyClass(6, 10);
            MyClass c6 = new MyClass();
            c1.display();
            c2.display();
            c3.display();
            c4.display();
            c5.display();
            c6.display();
            Console.ReadKey();
        }
    }
}
