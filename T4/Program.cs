﻿using System;
using System.Diagnostics;
using System.Text;

namespace T4
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dt = DateTime.Now;
            Process p = new Process();
            p.StartInfo.FileName = "notePad";
            p.StartInfo.WindowStyle=ProcessWindowStyle.Hidden;
            p.Start();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("notepad进程id："+p.Id+",启动时间："+dt);

            p.Close();
            Console.WriteLine(sb);
            Console.WriteLine("notepad进程已退出");
            Console.ReadKey();
        }
    }
}
