﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 异步编程基础_01
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            List<int> n1 = new List<int> { 5, 4, 7, 3, 9, 8, 6, 7, 2, 0 };
            sb.AppendFormat("List<int>中的数：{0}", string.Join(",", n1.ToArray()));
            var q1 = n1.Where(i => i < 4);
            sb.AppendFormat("\n小于4的数: {0}", string.Join("，", q1.ToArray()));
            sb.AppendLine("\n");
            //查询数组(找出每个数字对应的英文单词)
            int[] n2 = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };
            sb.AppendFormat("数字序列: {0}", string.Join("，", n2));
            string[] strings = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
            var q2 = n2.Select((n) => strings[n]);
            sb.AppendFormat(" \n数字对应的单词: {0}", string.Join("，", q2.ToArray()));
            //查询数组(找出所有偶数)
            var q3 = n2.Where((n) => n % 2 == 0);
            sb.AppendFormat("\n偶数: {0}", string.Join("，", q3.ToArray()));
            Console.WriteLine(sb);
            Console.ReadKey();

        }
    }
}
