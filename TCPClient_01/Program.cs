﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace TCPServer_01
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint iep = new IPEndPoint(IPAddress.Loopback, 2100);
            TcpListener listener = new TcpListener(iep);
            listener.Start();
            Console.WriteLine("TCP服务端在{0}监听...", iep);
            Task.Run(async () =>
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                EndPoint clientiep = client.Client.RemoteEndPoint;
                Console.WriteLine("TCP客户端终结点信息：{0}", clientiep);
                client.Close();
            });
            Console.ReadKey();
        }
    }
}
