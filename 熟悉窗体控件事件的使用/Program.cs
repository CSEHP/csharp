﻿using System;
using System.Windows.Forms;
namespace 熟悉窗体控件事件的使用
{
    public class Form1 : Form
    {
        int i;
        public Form1()
        {
            this.Click += Form1_Click1;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void Form1_Click1(object sender, EventArgs e)
        {
            this.Text = i.ToString();
            i++;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Application.Run(new Form1());
        }
    }
}
