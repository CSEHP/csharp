﻿using System;

namespace 属性get和set访问器
{
    class Student
    {
        private int age;
        public int Age
        {
            get { return age; }
            set
            {
                if (value >= 0) age = value;
                {

                }
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student();
            s.Age = -25;
            Console.WriteLine($"年龄：{s.Age}");
            Console.ReadKey();
        }
    }
}
