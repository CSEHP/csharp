﻿using System;
using System.Text;

namespace 数据的编码和解码_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "202015090000张二";
            Encoding gb2312 = Encoding.Default;
            Encoding utf8 = Encoding.UTF8;
            byte[] data1 = gb2312.GetBytes(str);
            byte[] data2 = utf8.GetBytes(str);
            Console.WriteLine("使用{0}编码的结果如下:", gb2312.BodyName);
            Console.WriteLine(BitConverter.ToString(data1));
            Console.WriteLine("使用{0}解码的结果如下:", gb2312.BodyName);
            Console.WriteLine(gb2312.GetString(data1));
            Console.WriteLine("-------------------------------");
            Console.WriteLine("使用{0}编码的结果如下:", utf8.BodyName);
            Console.WriteLine(BitConverter.ToString(data2));
            Console.WriteLine("使用{0}解码的结果如下:", utf8.BodyName);
            Console.WriteLine(utf8.GetString(data2));
            Console.WriteLine("-------------------------------");

            byte[] data3 = Encoding.Convert(gb2312, utf8, data1);
            Console.WriteLine("将字符串编码由gbk2312转为utf-8,输出结果: ");
            Console.WriteLine(utf8.GetString(data3));
            Console.ReadKey();

        }
    }
}
