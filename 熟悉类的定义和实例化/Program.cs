﻿using System;

namespace 熟悉类的定义和实例化
{
    class Program
    {
        public class TPoint
        {
            int x, y;
            public void setpoint(int x1, int y1)
            {
                x = x1;
                y = y1;
            }
            public void disPoint()
            {
                Console.WriteLine("{0},{1}", x, y);
            }
        }


        static void Main(string[] args)
        {
            TPoint p1 = new TPoint();
            p1.setpoint(2, 6);
            Console.WriteLine("第一个点=>");
            p1.disPoint();

            TPoint p2 = new TPoint();
            p2.setpoint(8, 3);
            Console.WriteLine("第二个点=>");
            p2.disPoint();

            Console.ReadKey();
        }


    }
}
