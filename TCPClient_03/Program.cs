﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TCPClient_03
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient();
            client.Connect(IPAddress.Parse("127.0.0.1"),3596);
            NetworkStream ns = client.GetStream();
            BinaryWriter bw = new BinaryWriter(ns);
            BinaryReader br = new BinaryReader(ns);
            int i;
            while (true){
                try {
                    
                        if (!int.TryParse(Console.ReadLine(),out i)) i = 0;
                        bw.Write(i);
                        Console.WriteLine(br.ReadString());

                    }catch (Exception e) {

                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    break;
                }
            }

        }
}
}
