﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TCPServer_03
{

    class Wash
    {
        public int no;
        public string name;
        public double price;
        public override string ToString()
        {
            return string.Format("货号: {0}， 货名: {1}， 价格: {2}", no, name, price);
        }


    }
    class Program
    {
        static void Main(string[] args)
        {



            List<Wash> list = new List<Wash>() {
new Wash() {no = 1,name = "牙刷",price = 9.9},
new Wash() {no = 2,name="牙膏",price = 15.6},
new Wash() {no = 3,name= "毛巾",price = 10},
new Wash() {no = 4,name ="沐浴露",price = 36.9},
new Wash() {no = 5,name ="洗发露",price = 45.9}
};

            IPEndPoint iep = new IPEndPoint(IPAddress.Any, 3596);
            TcpListener listener = new TcpListener(iep);
            listener.Start();
            Console.WriteLine("TCP服务器在{0}监......", iep);

            TcpClient client = listener.AcceptTcpClient();
            NetworkStream ns = client.GetStream();
            BinaryWriter bw = new BinaryWriter(ns);
            BinaryReader br = new BinaryReader(ns);
            int i = 0;
            while (true)
            {
                try
                {
                    i = br.ReadInt32();
                    Wash wash = list.Find(w => w.no == i);
                    if (wash != null) bw.Write(wash.ToString());
                    else bw.Write("没有找到");
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    break;

                }

            }
        }
    }
}
