﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPServer_03
{
    class Program
    {
        static void Main(string[] args)
        {

            IPEndPoint iep = new IPEndPoint(IPAddress.Loopback, 6312);
            UdpClient server = new UdpClient(iep);
            Console.WriteLine("UDP服务端在{0}等待接收信息...", iep);
            Random r = new Random();
            while (true)
            {
                try
                {
                    IPEndPoint remoteEp = null;
                    server.Receive(ref remoteEp);
                    byte[] data = new byte[4];
                    r.NextBytes(data);
                    server.Send(data, data.Length, remoteEp);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    break;
                }



            }
        }
    }
}

